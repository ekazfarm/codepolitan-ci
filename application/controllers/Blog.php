<?php

class Blog extends CI_Controller {

    public function index()
    {
        $data['blogs'] = [
            [
                'title' => 'Artikel Pertama',
                'content' => '<p> PDo excepteur labore id occaecat dolor. Excepteur proident dolor eu officia velit aliquip et proident sunt enim fugiat amet. 
                Lorem excepteur labore non eu dolor incididunt cillum aliqua ipsum. Fugiat nostrud adipisicing ea officia minim.
                </p>'
            ],
            [
                'title' => 'Artikel Kedua',
                'content' => '<p> PDo excepteur labore id occaecat dolor. Excepteur proident dolor eu officia velit aliquip et proident sunt enim fugiat amet. 
                Lorem excepteur labore non eu dolor incididunt cillum aliqua ipsum. Fugiat nostrud adipisicing ea officia minim.
                </p>'
            ],
            [
                'title' => 'Artikel Ketiga',
                'content' => '<p> PDo excepteur labore id occaecat dolor. Excepteur proident dolor eu officia velit aliquip et proident sunt enim fugiat amet. 
                Lorem excepteur labore non eu dolor incididunt cillum aliqua ipsum. Fugiat nostrud adipisicing ea officia minim.
                </p>'
            ],
        ];
        $this->load->view('blog', $data) ;

    }

}